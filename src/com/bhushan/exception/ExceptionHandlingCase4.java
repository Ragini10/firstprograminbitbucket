package com.bhushan.exception;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ExceptionHandlingCase4 {

	public static void main(String[] args) {

		File file = new File("C:\\Users\\admin\\Documents\\Ragini.txt");

		try {
			FileInputStream fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
}