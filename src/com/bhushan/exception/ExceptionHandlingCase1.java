package com.bhushan.exception;

public class ExceptionHandlingCase1 {

	public static void main(String[] args) {
		int number = 5, calculation;

		try {
			calculation = number / 0;
			System.out.println(calculation);
		} catch (ArithmeticException e) {
			System.out.println("Arithmetic Exception Occurred");
		} finally {
			System.out.println("Exception has been handled");
		}

	}
}
